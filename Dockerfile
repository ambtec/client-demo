FROM nginx
WORKDIR /usr/share/nginx/html

COPY index.html index.html
COPY favicon.png favicon.png
COPY socket.io.js socket.io.js
EXPOSE 80